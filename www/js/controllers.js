/* global angular, document, window */
'use strict';

angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $ionicPopover, $timeout, AuthService,$state) {
    // Form data for the login modal
    $scope.loginData = {};
    $scope.isExpanded = false;
    $scope.hasHeaderFabLeft = false;
    $scope.hasHeaderFabRight = false;

    var navIcons = document.getElementsByClassName('ion-navicon');
    for (var i = 0; i < navIcons.length; i++) {
        navIcons.addEventListener('click', function() {
            this.classList.toggle('active');
        });
    }

    ////////////////////////////////////////
    // Layout Methods
    ////////////////////////////////////////app

    $scope.hideNavBar = function() {
        document.getElementsByTagName('ion-nav-bar')[0].style.display = 'none';
    };

    $scope.showNavBar = function() {
        document.getElementsByTagName('ion-nav-bar')[0].style.display = 'block';
    };

    $scope.noHeader = function() {
        var content = document.getElementsByTagName('ion-content');
        for (var i = 0; i < content.length; i++) {
            if (content[i].classList.contains('has-header')) {
                content[i].classList.toggle('has-header');
            }
        }
    };

    $scope.setExpanded = function(bool) {
        $scope.isExpanded = bool;
    };

    $scope.setHeaderFab = function(location) {
        var hasHeaderFabLeft = false;
        var hasHeaderFabRight = false;

        switch (location) {
            case 'left':
                hasHeaderFabLeft = true;
                break;
            case 'right':
                hasHeaderFabRight = true;
                break;
        }

        $scope.hasHeaderFabLeft = hasHeaderFabLeft;
        $scope.hasHeaderFabRight = hasHeaderFabRight;
    };

    $scope.hasHeader = function() {
        var content = document.getElementsByTagName('ion-content');
        for (var i = 0; i < content.length; i++) {
            if (!content[i].classList.contains('has-header')) {
                content[i].classList.toggle('has-header');
            }
        }

    };

    $scope.hideHeader = function() {
        $scope.hideNavBar();
        $scope.noHeader();
    };

    $scope.showHeader = function() {
        $scope.showNavBar();
        $scope.hasHeader();
    };

    $scope.clearFabs = function() {
        var fabs = document.getElementsByClassName('button-fab');
        if (fabs.length && fabs.length > 1) {
            fabs[0].remove();
        }
    };

     $scope.email_id = AuthService.get_email();
     if ( $scope.email_id  !== null)

     {
      $scope.email = $scope.email_id.replace('@arrkgroup.com', '').replace('.', ' ');  
     }
     

     $scope.logout = function() {
            AuthService.logout();
            $state.go('app.login');
        };
})

.controller('LoginCtrl', function($scope, $timeout, ionicMaterialInk, $ionicPopup, AuthService, $state, $ionicLoading) {
    $scope.user = {
        email_id: '',
        password: ''
    };
    $scope.login = function() {
        console.log($scope.user);
        AuthService.set_email($scope.user.email_id);
        AuthService.login($scope.user).then(function(msg) {
            if (msg == '701') {
                $ionicLoading.hide();
                var alertPopup = $ionicPopup.alert({
                    title: 'User with email does not exist',
                    template: msg
                });
            } else {
                if (msg == '702') {
                    $ionicLoading.hide();
                    $state.go('app.feedback', {}, {
                        reload: true
                    });
                } else {
                    if (msg = '703') {
                        $ionicLoading.hide();
                        $state.go('otp', {}, {
                            reload: true
                        });
                    }

                }
            }

        }, function(errMsg) {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
                title: 'Login failed!',
                template: errMsg
            });
        });
    };



    $scope.$parent.clearFabs();

    $timeout(function() {
        $scope.$parent.hideHeader();
    }, 0);
    ionicMaterialInk.displayEffect();
})


    .controller('OtpCtrl', function($scope, $stateParams, $timeout, ionicMaterialInk, $ionicPopup, AuthService, $state, $ionicLoading, $http) {
        // Set Motion

        // Set Ink
        $scope.user = {
            otp: ''
        };
        $http.defaults.headers.post['My-Header'] = 'value';
        $http.defaults.headers.post["Content-Type"] = "application/json";
        $http.defaults.headers.common.static_token = "5b99f705b0486db23d82c5fa3779db8bcc9f9db93ee0a87f64698eb16c6c7e73";

        console.log($scope.email_id)

        $scope.sendotp = function() {

            $scope.email_id = AuthService.get_email();
            console.log($scope.email_id);
            var data = {
                email_id: $scope.email_id,
                otp: $scope.user.otp
            }
            console.log("dataa" + data)
            AuthService.send_otp(data).then(function(msg) {
                console.log(msg);
                if (msg == '200') {
                    $ionicLoading.hide();
                    var alertPopup = $ionicPopup.alert({
                        title: 'User is not activated',
                        template: msg
                    });
                } else {
                    if (msg == '705') {
                        $ionicLoading.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'OTP invalid',
                            template: msg
                        });
                    } else {
                        if (msg = '707') {
                            $ionicLoading.hide();
                            $state.go('project', {}, {
                                reload: true
                            });
                        }
                    }
                }

            }, function(errMsg) {
                $ionicLoading.hide();
                var alertPopup = $ionicPopup.alert({
                    title: 'Login failed!',
                    template: errMsg
                });
            });
        };
    })


.controller('ProjectCtrl', function($scope, $stateParams, $timeout, ionicMaterialMotion, $ionicPopup, $state, ionicMaterialInk, $http, AuthService, $ionicLoading) {
    $http.defaults.headers.post['My-Header'] = 'value';
    $http.defaults.headers.post["Content-Type"] = "application/json";
    $http.defaults.headers.common.static_token = "5b99f705b0486db23d82c5fa3779db8bcc9f9db93ee0a87f64698eb16c6c7e73";
    $scope.selectedTestAccount = null;
    $scope.testAccounts = [];


    $http({
        url: 'http://localhost:8080/project',
        method: 'GET'
    }).success(function(result) {
        $scope.testAccounts = result;
    }, function(error) {});

    $scope.user = {
        project: ''
    };
    $scope.send_dashboard = function() {
        $scope.email_id = AuthService.get_email();
        var data = {
            project: $scope.user.project.project_name,
            email_id: $scope.email_id

        }
        console.log("dataa" + data)
        AuthService.submit_project(data).then(function(msg) {
            console.log(msg);
            if (msg == '703') {
                $ionicLoading.hide();
                        $state.go('app.feedback', {}, {
                            reload: true
                        });
            }else{
                 $ionicLoading.hide();
                var alertPopup = $ionicPopup.alert({
                    title: 'Something wrong',
                    template: msg
                });
            }

        }, function(errMsg) {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
                title: 'Login failed!',
                template: errMsg
            });
        });
    };


})

.controller('signupCtrl', function($scope, $stateParams, $timeout, $state, ionicMaterialMotion, $ionicPopup, ionicMaterialInk, $http, AuthService, $ionicLoading) {
    $http.defaults.headers.post['My-Header'] = 'value';
    $http.defaults.headers.post["Content-Type"] = "application/json";
    $http.defaults.headers.common.static_token = "5b99f705b0486db23d82c5fa3779db8bcc9f9db93ee0a87f64698eb16c6c7e73";
    $scope.user = {
        email_id: '',
        password: ''
    };
    $scope.sign_up = function() {
        console.log($scope.user);
        var data = {
            name: $scope.user.name,
            emailid: $scope.user.email_id,
            password: $scope.user.password

        }
        console.log("dataa" + data)
        AuthService.sign_up(data).then(function(msg) {
            console.log(msg);
            if (msg == 'Invalid email pattern') {
                $ionicLoading.hide();
                var alertPopup = $ionicPopup.alert({
                    title: 'Invalid',
                    template: 'Please enter mandatory details'
                });
            } else {
                if (msg == '701-Email already exist. Please contact your administrator') {
                  $ionicLoading.hide();
                  var alertPopup = $ionicPopup.alert({
                    title: 'Invalid',
                    template: 'Email already exist'
                });
                } else {
                    if (msg = '703') {
                        $ionicLoading.hide();
                        $state.go('app.login', {}, {
                            reload: true
                        });
                    }
                }
            }

        }, function(errMsg) {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
                title: 'Login failed!',
                template: errMsg
            });
        });
    };


})



.controller('DashboardCtrl', function($scope, $stateParams, $timeout, ionicMaterialMotion, ionicMaterialInk) {

    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.isExpanded = true;
    $scope.$parent.setHeaderFab('right');

    // Activate ink for controller
    ionicMaterialInk.displayEffect();
})
.controller('FeedbackCompletedCtrl', function($scope, $stateParams, $timeout, ionicMaterialMotion, ionicMaterialInk) {

    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.isExpanded = true;
    $scope.$parent.setHeaderFab('right');

    // Activate ink for controller
    ionicMaterialInk.displayEffect();
})


.controller('FeedbackCtrl', function($scope, $stateParams, $timeout, ionicMaterialMotion, $ionicPopup, ionicMaterialInk, $http, AuthService, $ionicLoading, $state) {

    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.isExpanded = true;
    $http.defaults.headers.post["Content-Type"] = "application/json";
    $http.defaults.headers.common.dynamic_token = AuthService.get_token();
    $scope.selectedTestAccount = null;
    $scope.questions = [];
    $scope.email_id = AuthService.get_email();
    $scope.questionToShow = 0;
    $scope.showComment = 'blank';
    $scope.quotient_images = [{quotient: "happy", imageUrl: "../img/happy_inactive.png", active_image: "../img/happy_active.png", inactive_image: "../img/happy_inactive.png"},
                            {quotient: "blank", imageUrl: "../img/meh_inactive.png", active_image: "../img/meh_active.png", inactive_image: "../img/meh_inactive.png"},
                            {quotient: "sad", imageUrl: "../img/sad_inactive.png", active_image: "../img/sad_active.png", inactive_image: "../img/sad_inactive.png"}];

    $http({
        url: 'http://localhost:8080/questions?email='+$scope.email_id,
        method: 'GET'
    }).success(function(result) {
        $scope.questions = result;

        if ($scope.questions.length == 0){
            console.log("No questions Or All Question answered.")
            $state.go('app.completed', {}, {
                            reload: true
                        });
        }
    }, function(error) {

    });
    
    $scope.answer = {
        quotient: '',
        comments: '',
        question_id: '',
        email_id: $scope.email_id
    };

    $scope.quotientClick = function(media) {

        $scope.quotient_images.forEach(function(data)
        {
          data.imageUrl = data.inactive_image;
        });

        if (media.imageUrl == media.inactive_image){
            $scope.showComment = media.quotient;    
            media.imageUrl = media.active_image;
        }else{
            media.imageUrl = media.inactive_image;
            $scope.showComment = $scope.showComment;    
        }
        $scope.answer.quotient = media.quotient;
    };

    $scope.submit_user_answer = function() {

        var data = {
            ques_id: $scope.questions[$scope.questionToShow].id,
            email: $scope.email_id,
            quotient: $scope.answer.quotient,
            comments: $scope.answer.comments,
            cdate: new Date()
        };

        console.log(data)
        AuthService.submit_user_answer(data).then(function(msg) {
            console.log(msg)
            $ionicLoading.hide();
            if (msg == '703') {
                $ionicLoading.hide();
            }else {
                console.log(msg)
                if (msg == "Please enter mandatory details"){
                     var alertPopup = $ionicPopup.alert({
                        template: msg
                       });
                    $ionicLoading.hide();
                }else{
                    //If success
                    $scope.questionToShow = ($scope.questionToShow + 1) % $scope.questions.length;
                    $ionicLoading.hide();
                    $scope.quotient_images = [{quotient: "happy", imageUrl: "../img/happy_inactive.png", active_image: "../img/happy_active.png", inactive_image: "../img/happy_inactive.png"},
                            {quotient: "blank", imageUrl: "../img/meh_inactive.png", active_image: "../img/meh_active.png", inactive_image: "../img/meh_inactive.png"},
                            {quotient: "sad", imageUrl: "../img/sad_inactive.png", active_image: "../img/sad_active.png", inactive_image: "../img/sad_inactive.png"}];
                    $scope.answer = {
                        quotient: '',
                        comments: '',
                        question_id: '',
                        email_id: $scope.email_id
                    };
                    
                    if ($scope.questionToShow == 0){
                        $state.go('app.completed', {}, {
                            reload: true
                        });
                    }
                }    
            }
            
        }, function(errMsg) {
            $ionicLoading.hide();
        });
    }

});


